//1.
function ShippingContainer(weight, height, width, length, type) {
    this.weight = weight;
    this.height = height;
    this.width = width;
    this.length = length;
    this.type = type;
}

//2.
const containerBananas = new ShippingContainer(3000, 400, 150, 300, { "Bananas": 5 });
const containerCars = new ShippingContainer(3000, 400, 550, 500, { "Cars": 500000 });

//3. Returns the volume
function calculateVolume(container) {
    return container.height * container.width * container.length;
}

//4. Assuming this function should return a value as well. (Not specified in task 4).
function calculateValue(container) {
    return container.weight * Object.values(container.type);
}

//5.
function printContainerInfo(container) {
    console.log("Weight: " + container.weight + " Height: " + container.height + " Width: " + container.width + " Length: " + container.length + " Volume: " + calculateVolume(container) + " Value: " + calculateValue(container));
}

//6.
const container = new ShippingContainer(1000, 300, 300, 300, { "Apples": 29 });
printContainerInfo(container);
